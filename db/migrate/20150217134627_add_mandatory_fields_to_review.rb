class AddMandatoryFieldsToReview < ActiveRecord::Migration
  def change
    change_column :reviews, :content, :text, null: false
    change_column :reviews, :user_id, :integer, null: false
    change_column :reviews, :rating, :integer, null: false
  end
end
