class AddMandatoryFieldsToProduct < ActiveRecord::Migration
  def change
    change_column :products, :description, :text, null: false
    change_column :products, :title, :string, null: false
    change_column :products, :price, :float, scale: 2
  end
end
