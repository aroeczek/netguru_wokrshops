# ---Admin login---
# login: admin@email.com
# password: admin12345678

# ---Regular users login---
# login: user[:firstname]@email.com
# password: user[:firstname]12345678

categories = []
users = { firstname: 'John', lastname: 'Doe' },
        { firstname: 'Paul', lastname: 'Suie' },  
        { firstname: 'Peter', lastname: 'Doha' },
        { firstname: 'Rob', lastname: 'Lobs' },
        { firstname: 'Mark', lastname: 'White' }

admin = User.create!(email: 'admin@email.com', password: 'admin12345678', 
                    firstname: 'John', lastname: 'Done', 
                    about_me: "Hi, my name is John Done. I'm an administrator.", 
                    admin: true)
2.times do |i|
  categories << Category.create!(name: "Category #{i}")
end

users.each do |user|
  u = User.create!(email: "#{user[:firstname]}@email.com", password: "#{user[:firstname]}12345678", 
                    firstname: "#{user[:firstname]}", lastname: "#{user[:lastname]}", 
                    about_me: "Hi, my name is #{user[:firstname]} #{user[:lastname]}.")
  5.times do |i|
    u.products.create!(title: "Product #{i}", description: "Description #{i}", 
                       price: rand(1.0...20.0).round(2), category_id: categories[i%2].id)
  end                 
end

Product.all.each do |product|
  User.all.each do |user|
    2.times do |i|
      product.reviews.create!(content: "Product review #{i}", rating: rand(1..5), user: user)
    end
  end  
end 