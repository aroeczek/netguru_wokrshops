require 'spec_helper'

describe User do
  it { should validate_presence_of :firstname }
  it { should validate_presence_of :lastname }

  it "by default isn't admin" do
    expect(User.new).to_not be_admin
  end

  describe '#is_product_owner?' do
    let(:user)          { build :user }
    let(:other_user)    { build :user }

    before do
      @product = build :product, user: user
    end

    context 'when user is product owner' do
      it 'returns true' do
        expect(user.is_product_owner?(@product)).to eq true
      end
    end
  
    context 'when user is not product owner' do
      before do
        @product.user = other_user
      end

      it 'returns true' do
        expect(user.is_product_owner?(@product)).to eq false
      end
    end
  
  end

end
