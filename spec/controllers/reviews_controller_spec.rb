require 'spec_helper'

describe ReviewsController do
  describe 'POST create' do
    let(:user) { create :user }
    let(:other_user) { create :user }
    let(:review) { attributes_for :review }
    let(:category) { create :category }
    let(:product) { create :product }

    before do
      login_user user
    end

    it 'creates new review' do 
      expect {
            post :create, { review: review, category_id: category, product_id: product }
          }.to change(Review, :count).by(1)
    end

    it 'assigns review to the user' do
      expect {
            post :create, { review: review, category_id: category, product_id: product }
          }.to change(user.reload.reviews, :count).by(1)
    end

    it 'does not assigns review to other user' do
      expect {
            post :create, { review: review, category_id: category, product_id: product }
          }.to change(other_user.reload.reviews, :count).by(0)
    end

  end
end