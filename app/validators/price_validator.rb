class PriceValidator < ActiveModel::EachValidator
  
  PRICE_REGEX =  /\A[+]?\d+(.\d{1,2}){0,1}\z/

  def validate_each(record, attribute, value)
    unless value.to_s =~ PRICE_REGEX
      record.errors[attribute] << (options[:message] || "wrong price format")
    end
  end
end