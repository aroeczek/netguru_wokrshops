class Product < ActiveRecord::Base
  belongs_to :category
  belongs_to :user
  has_many :reviews

  validates :description, :price, :title, presence: true
  validates :price, numericality: { greater_than: 0 }
  validates :price, price: true

  paginates_per 15 

  def average_rating
    avg = reviews.any? ? reviews.pluck(:rating).reduce(:+).to_f / reviews.size : 0.0 
    avg.round(2)
  end 

end
