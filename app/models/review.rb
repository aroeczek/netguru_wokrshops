class Review < ActiveRecord::Base
  belongs_to :product
  belongs_to :user

  validates :rating, :user_id, :content, presence: true
  
  paginates_per 5
  
  scope :most_recent, ->(amount = 10) { order("created_at DESC").limit(amount) }

end
