class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :firstname, :lastname, :email, presence: true       

  has_many :reviews
  has_many :products


  def is_product_owner?(product)
    self == product.user
  end

end
