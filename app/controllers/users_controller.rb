class UsersController < ApplicationController
  expose_decorated(:users)
  expose_decorated(:user)
  expose_decorated(:recent_reviews, model: :review, 
    decorator: ReviewDecorator) { user.reviews.most_recent(RECENT_USER_REVIEWS) }

  RECENT_USER_REVIEWS = 5
 
  def show
  end

  def index
  end

end
