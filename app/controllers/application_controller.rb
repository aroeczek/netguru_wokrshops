class ApplicationController < ActionController::Base
  # rescue_from ActiveRecord::RecordNotFound, :with => :resource_not_found

  before_action :configure_permitted_parameters, if: :devise_controller?

  decent_configuration do
    strategy DecentExposure::StrongParametersStrategy
  end

  protect_from_forgery with: :exception

  def acknowledge_product_owner
    unless current_user.is_product_owner?(product)
      redirect_to(category_product_url(category, product), 
        flash: {:error => 'You are not allowed to edit this product.'})
    end
  end
  
  def approve_admin!
    unless current_user
      redirect_to new_user_session_path, 
        flash: {:error => 'You should be logged as an admin.'}
    end

    unless current_user.admin?
      redirect_admin_to_back flash: {:error => 'You should be logged as an admin.'}
    end
  end

  def current_user
    UserDecorator.decorate(super) unless super.nil?
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << [ :firstname, :lastname, :about_me ]
    devise_parameter_sanitizer.for(:account_update) << [ :firstname, :lastname, :about_me ]
  end

  private

  def redirect_admin_to_back(default = new_user_session_url, flash: {})
    if !request.env["HTTP_REFERER"].blank? and request.env["HTTP_REFERER"] != request.env["REQUEST_URI"]
      redirect_to :back, flash: flash
    else
      redirect_to default, flash: flash
    end
  end

  def resource_not_found
    redirect_to root_path, flash: {:error => 'Requested resource not found.'}
  end
  
end
