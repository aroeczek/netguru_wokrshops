class ProductsController < ApplicationController
  expose(:category)
  expose(:products) { category.products.page params[:page] }
  expose(:product) { params[:id] ? Product.find(params[:id]) : Product.new }
  expose(:review) { Review.new }
  expose_decorated(:reviews, ancestor: :product) { product.reviews.page params[:page] }

  before_action :authenticate_user!
  before_action :acknowledge_product_owner, only: [:edit, :update, :destroy]  

  def index
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    params.fetch(:product, []).fetch(:price, '').gsub!(',', '.')
    self.product = Product.new(product_params)
    self.product.user = current_user

    if product.save
      category.products << product
      redirect_to category_product_url(category, product), notice: 'Product was successfully created.'
    else
      render action: 'new'
    end
  end

  def update
    if self.product.update(product_params)
      redirect_to category_product_url(category, product), notice: 'Product was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /products/1
  def destroy
    product.destroy
    redirect_to category_url(product.category), notice: 'Product was successfully destroyed.'
  end

  private

  def product_params
    params.require(:product).permit(:title, :description, :price, :category_id)
  end

end
