// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$( document ).ready(function() {
  var starCaptions = {
                      1: 'Very Poor',
                      2: 'Poor',
                      3: 'OK',
                      4: 'Good',
                      5: 'Very Goog'
                    };

  $("#review_rating").rating({min: 0, max: 5, step: 1, size: 'xs', starCaptions: starCaptions});
})

