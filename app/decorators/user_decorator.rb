class UserDecorator < ApplicationDecorator
  delegate_all

  def full_name
     "#{object.firstname} #{object.lastname}"
  end

  def reviews_count
    self.reviews.size
  end

end