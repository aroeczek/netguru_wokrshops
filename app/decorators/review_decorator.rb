class ReviewDecorator < ApplicationDecorator
  delegate_all
  
  NUMBER_OF_CHARACTERS = 200

  def author
    "#{object.user.firstname} #{object.user.lastname}"
  end

  def short_content
    object.content.truncate(NUMBER_OF_CHARACTERS, separator: ' ')
  end
end
