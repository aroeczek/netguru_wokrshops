Rails.application.routes.draw do
  
  root 'categories#index'

  resources :categories do
    resources :products do
      resources :reviews
    end
  end

  devise_for :users

  resources :users, only: [:show, :index]
end
